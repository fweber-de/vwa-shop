<?php

namespace App\Model;

class Product
{
    const TABLE_NAME = 'Lebensmittel';

    public $id;
    public $name;
    public $preis;
}
