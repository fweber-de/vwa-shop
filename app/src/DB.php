<?php

namespace App;

class DB
{
    private $host;
    private $db;
    private $username;
    private $password;

    public function __construct($host, $db, $username, $password)
    {
        $this->host = $host;
        $this->db = $db;
        $this->username = $username;
        $this->password = $password;
    }

    private function connect()
    {
        $handle = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->username, $this->password, [
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ]);

        return $handle;
    }

    public function openTable($sql)
    {
        $connection = $this->connect();

        $query = $connection->query($sql);

        if (!$query) {
            throw new \Exception('The Query couldn\'t be executed!');
        }

        return $query;
    }

    public function executeSQL($sql)
    {
        $this->openTable($sql);

        return $this;
    }

    public function executeQuery($sql)
    {
        $query = $this->openTable($sql);

        return $query->fetch()[0];
    }
}
