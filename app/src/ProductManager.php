<?php

namespace App;

use App\Model\Product;
use App\Model\Order;

class ProductManager
{
    private $db;

    public function __construct($db) //db objekt übergeben bekommen
    {
        $this->db = $db;
    }

    public function getProducts()
    {
        //abfrage an db server senden
        $query = $this->db->openTable('select * from '.Product::TABLE_NAME);
        $query->setFetchMode(\PDO::FETCH_ASSOC);

        //productliste deklarieren
        $products = [];

        //für jedes ergebnis der abfrage
        foreach ($query as $row) {
            $product = new Product(); //neue instanz der klasse Product

            //werte aus db in product schreiben
            $product->id = $row['id'];
            $product->name = $row['Produktname'];
            $product->preis = $row['Preis'];

            $products[] = $product; //product object der liste hinzufügen
        }

        //liste rausgeben
        return $products;
    }

    public function buyProduct(Order $order)
    {
        $this->db->executeSQL('insert into '.Order::TABLE_NAME.' (Produkt_id, Menge) values ('.$order->productId.', '.$order->menge.')');
    }

    public function getOrders()
    {
        //abfrage an db server senden
        $query = $this->db->openTable('select * from '.Order::TABLE_NAME);
        $query->setFetchMode(\PDO::FETCH_ASSOC);

        //orderliste deklarieren
        $orders = [];

        //für jedes ergebnis der abfrage
        foreach ($query as $row) {
            $order = new Order(); //neue instanz der klasse Order

            //werte aus db in order schreiben
            $order->id = $row['id'];
            $order->productId = $row['Produkt_id'];
            $order->menge = $row['Anzahl'];

            $orders[] = $order; //product object der liste hinzufügen
        }

        //liste rausgeben
        return $orders;
    }
}
