function refreshProducts() {
    let list_url = '?api=1&method=list';
    let products = [];

    $('#artikel').html('laden...');

    fetch(list_url, {})
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
        products = data;

        let html = '<table class="table table-striped">';
        html += '<tbody>';

        $.each(products, function(i, product) {
            html += '<tr>';

            html += '<td>' + product.name + '</td>';
            html += '<td>' + product.preis + '</td>';

            html += '</tr>';
        });

        html += '</tbody>';
        html += '</table>';

        $('#artikel').html(html);
    });
}

$(document).ready(function() {

    refreshProducts();

    $('#refresh_list').click(function(e) {
        refreshProducts();
    });

});
