<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use App\ProductManager;
use App\DB;

$loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/../templates');
$twig = new \Twig\Environment($loader);

$request = Request::createFromGlobals();

if($request->get('api', 0) == 1) {

    $method = $request->get('method');

    $db = new DB('185.128.118.24', 'Produkt', 'Nutzer', 'vwatifwab42');
    //$db = new DB('mysql', 'produkt', 'root', null);
    $pm = new ProductManager($db);

    if($method == 'list') {
        $products = $pm->getProducts();

        echo json_encode($products);
    } elseif($method == 'buy') {

    } elseif($method == 'list_orders') {
        $orders = $pm->getOrders();

        echo json_encode($orders);
    }

} else {
    echo $twig->render('index.html.twig');
}
