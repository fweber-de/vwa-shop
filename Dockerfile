FROM ubuntu:18.04

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get install -y  \
      software-properties-common \
      build-essential \
      curl \
      git \
      unzip \
      apache2 \
      php7.2 \
      php7.2-cli \
      libapache2-mod-php7.2 \
      php7.2-gd \
      php7.2-json \
      php7.2-ldap \
      php7.2-curl \
      php7.2-mbstring \
      php7.2-mysql \
      php7.2-xml \
      php7.2-xsl \
      php7.2-zip \
      php7.2-dev

COPY config/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY config/run /usr/local/bin/run

RUN chmod +x /usr/local/bin/run
RUN a2enmod rewrite
RUN a2enmod headers

RUN usermod -u 1000 www-data

RUN apt-get install -y locales && \
    echo "de_DE.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
COPY . /var/www/html

WORKDIR /var/www/html/app
RUN composer install

EXPOSE 80
CMD ["/usr/local/bin/run"]
